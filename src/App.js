import Layout from "./components/Layout/Layout";
import {Button, CircularProgress, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {fetchFood, openModal} from "./store/actions/dishesActions";
import {decrease, increase} from "./store/actions/cartActions";
import Modal from "./components/Modal/Modal";

const useStyles = makeStyles(theme => ({
    container: {
        marginTop: theme.spacing(3),
    },
    foods: {
        padding: theme.spacing(1)
    },
    cart : {
        padding: theme.spacing(6)
    },
    image: {
        width: theme.spacing(12),
        height: theme.spacing(10)
    }
}))

const App = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const food = useSelector(state => state.dishes.food);
    const count = useSelector(state => state.cart)
    const loading = useSelector(state => state.dishes.fetchLoading)
    const [price, setPrice] = useState(150);

    useEffect(() => {
        dispatch(fetchFood());
    }, [dispatch]);

    const foods = () => {
        if(!food) return;
        return Object.keys(food).map(fo => food[fo]);
    };
    const increaseCount = f => {
        const name = f.toLowerCase();
        Object.keys(food).map(fo => {
            if(fo === name){
                setPrice(prev => prev + food[fo].price)
            }
            return null;
        })
        dispatch(increase(name));
    }

    const decreaseCount = f => {
        const name = f.toLowerCase();
        Object.keys(food).map(fo => {
            if(fo === name){
                setPrice(prev => prev - food[fo].price)
            }
            return null;
        })
        dispatch(decrease(name));
    }

    const counts = () => {
       return Object.keys(count).map(c => {
           if(count[c] <= 0) return null;
           return (
               <Paper key={c}>
                   <Typography onClick={() => decreaseCount(c)} variant='body1' >{c} x<span>{count[c]}</span></Typography>
               </Paper>
           )
       })
    };

    return (
         <Layout>
             {loading ? <CircularProgress/> :
                 <><Grid  container justifyContent='center'  spacing={2} className={classes.container}>
                     <Grid item>
                         {foods() && foods().map(food =>(
                             <Paper key={food.name}>
                                 <Grid container alignItems='center'>
                                     <Grid item>
                                         <img className={classes.image} src={food.image} alt="typeOfFood"/>
                                     </Grid>
                                     <Grid item className={classes.foods}>
                                         <Paper className={classes.container}>
                                             <Typography variant='body1' >{food.name}</Typography>
                                             <Typography variant='h5' >KGZ {food.price}</Typography>
                                         </Paper>
                                     </Grid>
                                     <Grid item>
                                         <Button  onClick={() => increaseCount(food.name)} type="submit" variant='contained' color='primary'>Add to cart</Button>
                                     </Grid>
                                 </Grid>
                             </Paper>
                         ))}
                     </Grid>
                     <Grid item>
                         <Paper>
                             <Grid className={classes.cart} container direction='column'>
                                 <Typography variant='h4'>Cart</Typography>
                                 <Grid item className={classes.foods}>
                                     {counts()}
                                     <Typography variant='body1' >Delivery: 150</Typography>
                                     <Typography variant='body1' >Total: <span>{price}</span></Typography>
                                 </Grid>
                                 <Grid item>
                                     <Button onClick={() => dispatch(openModal())} disabled={price < 151} className={classes.foods} type="submit" variant='contained' color='primary'>Place order</Button>
                                 </Grid>
                             </Grid>
                         </Paper>
                     </Grid>
                 </Grid>
                 <Modal setPrice={setPrice}/></>
             }
        </Layout>
    )
};

export default App;
