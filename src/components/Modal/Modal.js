import React, { useState} from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    TextField
} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchOrder} from "../../store/actions/dishesActions";
import {removeState} from "../../store/actions/cartActions";

const ProfileDialog = ({setPrice}) => {
    const dispatch = useDispatch();
    const open = useSelector(state => state.dishes.modal);
    const count = useSelector(state => state.cart);

    const [formProfile, setFormProfile] = useState({
        firstName: '',
        phoneNumber: '',
        address: '',
    })




    const onSubmit = e => {
        e.preventDefault();
        dispatch(fetchOrder(formProfile, count));
        dispatch(removeState());
        setPrice(150)
    }

    const onInputChange = event => {
        const {name, value} = event.target;
        setFormProfile(prev => ({...prev, [name]: value}))
    }
    return (
        <Dialog open={open} aria-labelledby="form-dialog-title">
            <form onSubmit={onSubmit}>
                <DialogTitle id="form-dialog-title">Form to order</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        You fill yhis form and order
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        variant='outlined'
                        label="First Name"
                        type="text"
                        value={formProfile.firstName}
                        fullWidth
                        name='firstName'
                        onChange={onInputChange}
                    />
                    <TextField
                        margin="dense"
                        variant='outlined'
                        label="Phone Number"
                        type="text"
                        name='phoneNumber'
                        value={formProfile.phoneNumber}
                        fullWidth
                        onChange={onInputChange}
                    />
                    <TextField
                        margin="dense"
                        variant='outlined'
                        label="Address"
                        type="text"
                        name='address'
                        value={formProfile.address}
                        fullWidth
                        onChange={onInputChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button type='submit' color="primary">
                        Create Order
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
};

export default ProfileDialog;