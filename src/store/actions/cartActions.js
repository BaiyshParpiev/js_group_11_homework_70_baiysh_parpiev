export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const REMOVE = 'REMOVE';


export const increase = name => ({type: INCREASE, name: name});
export const decrease = name => ({type: DECREASE, name: name});
export const removeState = () => ({type: REMOVE})