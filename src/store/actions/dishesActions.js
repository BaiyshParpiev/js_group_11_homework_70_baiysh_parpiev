import {axiosApi} from "../../config";

export const FETCH_FOODS_REQUEST = 'FETCH_FOODS_REQUEST';
export const FETCH_FOODS_SUCCESS = 'FETCH_FOODS_SUCCESS';
export const FETCH_FOODS_FAILURE = 'FETCH_FOODS_FAILURE';

export const FETCH_ORDER_REQUEST = 'FETCH_ORDER_REQUEST';
export const FETCH_ORDER_SUCCESS = 'FETCH_ORDER_SUCCESS';
export const FETCH_ORDER_FAILURE = 'FETCH_ORDER_FAILURE';

export const OPEN_MODAL = 'OPEN_MODAL';

export const fetchFoodsRequest = () => ({type: FETCH_FOODS_REQUEST});
export const fetchFoodsSuccess = foods => ({type: FETCH_FOODS_SUCCESS, payload: foods});
export const fetchFoodsFailure = error => ({type: FETCH_FOODS_FAILURE, payload: error});

export const fetchOrderRequest = () => ({type: FETCH_ORDER_REQUEST});
export const fetchOrderSuccess = foods => ({type: FETCH_ORDER_SUCCESS, payload: foods});
export const fetchOrderFailure = error => ({type: FETCH_ORDER_FAILURE, payload: error});

export const openModal = () => ({type: OPEN_MODAL});

export const fetchFood = () => {
    return async dispatch => {
        try{
           dispatch(fetchFoodsRequest());
           const response = await axiosApi.get('/food.json');
           dispatch(fetchFoodsSuccess(response.data));
        }catch(e){
            dispatch(fetchFoodsFailure(e))
        }
    }
}
export const fetchOrder = (orderData, count) => {
    return async dispatch => {
        try{
            dispatch(fetchOrderRequest());
            const response = await axiosApi.post('/order.json', {...orderData, order: count});
            dispatch(fetchOrderSuccess(response.data));
        }catch(e){
            dispatch(fetchOrderFailure(e))
        }
    }
}


