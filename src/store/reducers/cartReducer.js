import {DECREASE, INCREASE, REMOVE} from "../actions/cartActions";

const initialState = {
    pilav: 0,
    lepeshka: 0,
    shakarap: 0,
};

const cartReducer = (state = initialState, action) => {
    switch (action.type){
        case INCREASE:
            return {...state, [action.name]: state[action.name] + 1};
        case DECREASE:
            return {...state, [action.name]: state[action.name] - 1};
        case REMOVE:
            return initialState;
        default:
            return state;
    }
}

export default cartReducer;