import {
    FETCH_FOODS_FAILURE,
    FETCH_FOODS_REQUEST,
    FETCH_FOODS_SUCCESS, FETCH_ORDER_FAILURE,
    FETCH_ORDER_REQUEST, FETCH_ORDER_SUCCESS, OPEN_MODAL
} from "../actions/dishesActions";

const initialState = {
    modal: false,
    fetchLoading: false,
    error: null,
    food: null,
};

const dishesReducer = (state = initialState, action) => {
    const add = (state, action) => {
        return {...state, food: {...action.payload}, fetchLoading: false};
    }
    switch (action.type){
        case OPEN_MODAL:
            return {...state, modal: true}
        case FETCH_FOODS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_FOODS_SUCCESS:
           return add(state, action);
        case FETCH_FOODS_FAILURE:
            return {...state, error: action.payload, fetchLoading: false}
        case FETCH_ORDER_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ORDER_SUCCESS:
            return {...state, fetchLoading: false, modal: false};
        case FETCH_ORDER_FAILURE:
            return {...state, error: action.payload, fetchLoading: false}
        default:
            return state;
    }
}

export default dishesReducer;